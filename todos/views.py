from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import CreateForm, CreateItemForm


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "list_detail": list_detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            name = form.save(False)
            name.save()
            return redirect("todo_list_detail", id=name.id)
    else:
        form = CreateForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = CreateForm(request.POST, instance=list_detail)
        if form.is_valid():
            list_detail = form.save()
            return redirect("todo_list_detail", id=list_detail.id)
    else:
        form = CreateForm(instance=list_detail)
    context = {
        "list_detail": list_detail,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    list_detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_detail.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = CreateItemForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = CreateItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_item_update(request, id):
    item_detail = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = CreateItemForm(request.POST, instance=item_detail)
        if form.is_valid():
            item_detail = form.save()
            return redirect("todo_list_detail", id=item_detail.id)
    else:
        form = CreateItemForm(instance=item_detail)
    context = {
        "item_detail": item_detail,
        "form": form,
    }
    return render(request, "todos/items/edit.html", context)
